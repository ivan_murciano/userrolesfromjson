﻿using UserRolesJson.Entities;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace UserRolesJson.WebUI.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Get User from web api
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        protected async Task<User> GetUser(string username, string password)
        {
            User user = null;
            string apiUrl = string.Format("{0}/User/{1}/{2}", ConfigurationManager.AppSettings["ApiUrl"], username, password); ;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", username, password));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(data);
                }
            }

            return user;

        }
    }
}