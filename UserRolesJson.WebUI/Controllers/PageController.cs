﻿using UserRolesJson.WebUI.Filters;
using System.Web.Mvc;

namespace UserRolesJson.WebUI.Controllers
{
    [Authorize]
    public class PageController : Controller
    {
        [Page1Authorize]
        public ActionResult PageOne()
        {
            return View();
        }

        [Page2Authorize]
        public ActionResult PageTwo()
        {
            return View();
        }

        [Page3Authorize]
        public ActionResult PageThree()
        {
            return View();
        }
    }
}