﻿using UserRolesJson.BL.Strategy;
using UserRolesJson.Entities;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace UserRolesJson.WebUI.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(string username, string password)
        {

            User user = await this.GetUser(username, password);

            if (user == null)
            {
                ViewBag.Errors = "Usuario no válido.";
                return View("Index");
            }
            else if (user.Roles ==null ||user.Roles.Count == 0)
            {
                ViewBag.Errors = "Usuario sin rol asignado.";
                return View("Index");
            }
            else
            {
                FormsAuthentication.SetAuthCookie(user.Username, false);
                Session["user"] = user;
                Context context = new Context();
                var url = context.GetRedirect(user.Roles.First().Name);

                return Redirect(url);
            }
        }

        [Authorize]
        public ActionResult LogOut()
        {
            if (Request.IsAuthenticated)
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
            }

            Session.Clear();
            return Redirect("/Account/Index/");

        }
    }
}