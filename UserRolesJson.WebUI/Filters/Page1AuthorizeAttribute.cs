﻿using UserRolesJson.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserRolesJson.WebUI.Filters
{
    public class Page1AuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (authorized)
            {
                var user = HttpContext.Current.Session["user"] as User;
                if (user == null || (user != null && !user.Roles.Any(r => r.Name == "PAGE_1")))
                    authorized = false;
            }

            return authorized;

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpForbiddenResult();

        }
    }
}