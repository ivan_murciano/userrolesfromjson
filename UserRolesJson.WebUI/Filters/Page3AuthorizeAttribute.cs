﻿using UserRolesJson.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UserRolesJson.WebUI.Filters
{
    public class Page3AuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (authorized)
            {
                var user = HttpContext.Current.Session["user"] as User;
                if (user == null || (user != null && !user.Roles.Any(r => r.Name == "PAGE_3")))
                    authorized = false;
            }

            return authorized;

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpForbiddenResult();
            //if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            //{
            //    filterContext.Result = new HttpForbiddenResult();
            //}
            //else
            //{
            //    filterContext.Result = new HttpUnauthorizedResult();
            //}
        }
    }
}