﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UserRolesJson.BL.Strategy;

namespace UserRolesJson.BL.Tests
{
    [TestClass]
    public class When_Calling_GetRedirect
    {
        private Context context;

        [TestMethod]
        [TestCategory("UnitTest"), TestCategory("Schibsted.Proof.Strategy")]
        [ExpectedException(typeof(ArgumentException))]
        public void then_If_page_Is_Null_Throw_New_ArgumentException()
        {
            string page = null;
            context = new Context();
            context.GetRedirect(page);

        }

        [TestMethod]
        [TestCategory("UnitTest"), TestCategory("Schibsted.Proof.Strategy")]
        public void then_If_page_Is_empty_Returns_Home_Index()
        {
            string page = string.Empty;
            context = new Context();
            var result = context.GetRedirect(page);
            var expected = "/Home/Index";
            Assert.AreEqual(expected, result);

        }

        [TestMethod]
        [TestCategory("UnitTest"), TestCategory("Schibsted.Proof.Strategy")]
        public void then_If_page_Is_not_in_dictionary_Returns_Home_Index()
        {
            string page = "una prueba";
            context = new Context();
            var result = context.GetRedirect(page);
            var expected = "/Home/Index";
            Assert.AreEqual(expected, result);

        }
    }
}
