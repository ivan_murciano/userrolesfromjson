﻿using UserRolesJson.DA.Interfaces;
using UserRolesJson.WebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserRolesJson.WebApi.Security
{
    public class ApiSecurity: IApiSecurity
    {
        private readonly IUserDA _userDA;

        public ApiSecurity(IUserDA userDA)
        {
            this._userDA = userDA;

        }
        public bool VaidateUser(string username, string password)
        {

            var user = _userDA.GetUser(username, password);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsAdmin(string username, string password)
        {
            var user = _userDA.GetUser(username, password);
            if (user != null && user.Roles.Any(r => r.Name == "ADMIN"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}