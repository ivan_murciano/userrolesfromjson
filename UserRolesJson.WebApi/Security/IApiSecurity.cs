﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserRolesJson.WebApi.Security
{
    public interface IApiSecurity
    {
        bool VaidateUser(string username, string password);
        bool IsAdmin(string username, string password);
    }
}