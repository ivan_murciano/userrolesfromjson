﻿using UserRolesJson.DA.Interfaces;
using UserRolesJson.WebApi.Security;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Filters;

namespace UserRolesJson.WebApi.Filters
{
    public class AdminAuthenticationAttribute : AuthorizationFilterAttribute
    {
        public IUserDA userDA { get; set; }
        public IApiSecurity apiSecurity { get; set; }


        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            else
            {
                // Gets header parameters  
                string authenticationString = actionContext.Request.Headers.Authorization.Parameter;
                string originalString = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationString));

                // Gets username and password  
                string usrename = originalString.Split(':')[0];
                string password = originalString.Split(':')[1];

                // Validate username and password  
                if (!apiSecurity.IsAdmin(usrename, password))
                {
                    // returns unauthorized error  
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Access denied");
                }
            }

            base.OnAuthorization(actionContext);
        }
    }
}