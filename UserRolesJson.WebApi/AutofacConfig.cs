﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.ApplicationServices;
using UserRolesJson.DA.Impl;
using UserRolesJson.DA.Interfaces;
using UserRolesJson.WebApi.Filters;
using System.Web.Http;
using Autofac.Integration.WebApi;
using System.Reflection;
using UserRolesJson.WebApi.Security;

namespace UserRolesJson.WebApi
{
    public class AutofacConfig
    {

        public static void Register(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<UserDA>().As<IUserDA>();
            builder.RegisterType<RolDA>().As<IRolDA>();

            builder.RegisterType<ApiSecurity>().As<IApiSecurity>();

            builder.RegisterType<BasicAuthenticationAttribute>().PropertiesAutowired();
            builder.RegisterType<AdminAuthenticationAttribute>().PropertiesAutowired();
            builder.RegisterWebApiFilterProvider(config);
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            config.DependencyResolver = resolver;
        }
    }





}