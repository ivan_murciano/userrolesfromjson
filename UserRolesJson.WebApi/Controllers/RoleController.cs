﻿using UserRolesJson.DA.Interfaces;
using UserRolesJson.WebApi.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UserRolesJson.WebApi.Controllers
{


    public class RoleController : ApiController
    {
        private readonly IUserDA _userDA;
        private readonly IRolDA _rolDA;


        public RoleController(IUserDA userDA, IRolDA rolDA)
        {
            this._userDA = userDA;
            this._rolDA = rolDA;
        }


        [AdminAuthentication]
        [Route("api/Role/{username}/{rol}")]
        [HttpPost]
        public HttpResponseMessage AddRoleToUser(string username, string rol)
        {
            if (!_userDA.UserExists(username))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User not found.");
            }
            else
            {
                var user = _rolDA.AddRoleToUser(username, rol);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
        }


        [AdminAuthentication]
        [Route("api/Role/{username}")]
        [HttpDelete]
        public HttpResponseMessage DeleteAllUserRoles(string username)
        {
            if (!_userDA.UserExists(username))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User does not exists.");
            }
            else
            {
                var user = _rolDA.DeleteAllUserRoles(username);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
        }
    }
}