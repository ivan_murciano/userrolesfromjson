﻿using UserRolesJson.DA;
using UserRolesJson.DA.Interfaces;
using UserRolesJson.Entities;
using UserRolesJson.WebApi.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UserRolesJson.WebApi.Controllers
{


    public class UserController : ApiController
    {
        private readonly IUserDA _userDA;

        public UserController(IUserDA userDA)
        {
            this._userDA = userDA;
        }

        [BasicAuthentication]
        [Route("api/User/{username}/{password}")]
        [HttpGet]
        public User GetUser(string username, string password)
        {
            return _userDA.GetUser(username, password);
        }

        [AdminAuthentication]
        [Route("api/User/{username}/{password}")]
        [HttpPost]
        public HttpResponseMessage CreatetUser(string username, string password)
        {
            if (_userDA.UserExists(username))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User already exists.");
            }
            else
            {
                var user = _userDA.CreateUser(username, password);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
        }

        [AdminAuthentication]
        [Route("api/User/{username}/{password}")]
        [HttpPut]
        public HttpResponseMessage MofifyUser(string username, string password)
        {
            if (!_userDA.UserExists(username))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User does not exists.");
            }
            else
            {
                var user = _userDA.ModifyUser(username, password);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
        }

        [AdminAuthentication]
        [Route("api/User/{username}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUser(string username)
        {
            if (!_userDA.UserExists(username))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User does not exists.");
            }
            else
            {
                var user = _userDA.DeleteUser(username);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
        }
    }
}