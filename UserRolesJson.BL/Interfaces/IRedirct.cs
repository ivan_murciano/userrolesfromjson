﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRolesJson.BL.Interfaces
{
    public interface IRedirect
    {
        string GetRedirect();
    }
}
