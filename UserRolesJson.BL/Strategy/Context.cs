﻿using UserRolesJson.BL.Interfaces;
using UserRolesJson.BL.Strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRolesJson.BL.Strategy
{
    public class Context
    {

        private Dictionary<string, IRedirect> _strategies = new Dictionary<string, IRedirect>();

        public Context()
        {
            _strategies.Add("PAGE_1", new PageOne());
            _strategies.Add("PAGE_2", new PageTwo());
            _strategies.Add("PAGE_3", new PageThree());

        }

        public string GetRedirect(string page)
        {   
            var defaultUrl = "/Home/Index";

            if (page == null)
                throw new ArgumentException("EL parámetro page no pude ser nulo");

            if (_strategies.ContainsKey(page))
                return _strategies[page].GetRedirect();
            else
                return defaultUrl;



        }
    }
}
