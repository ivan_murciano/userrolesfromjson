﻿using UserRolesJson.BL.Interfaces;
using System.Web;

namespace UserRolesJson.BL.Strategy
{
    public class PageTwo : IRedirect
    {
        public string GetRedirect()
        {
            return "/Page/PageTwo";
        }
    }
}