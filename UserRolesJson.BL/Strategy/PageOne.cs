﻿using UserRolesJson.BL.Interfaces;
using System.Web;

namespace UserRolesJson.BL.Strategy
{
    public class PageOne : IRedirect
    {
        public string GetRedirect()
        {
            return "/Page/PageOne";
        }
    }
}