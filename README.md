
 UserRolesJson README file
---------------------------------------------------------------

The application is implemented with Visual Studio 2017 and has two start projects:
  -UserRolesJson.WebUI (MVC project)
  -UserRolesJson.WebApi (Web API project, is setup to start at http://localhost:5001/)

The task of the application is to redirect the user to certain page depending on his role once loged in 
succesfully.
The users and their roles are stored in a json file located on UserRoles.WebApi\App_Data\users.json.
The webapi is responsible to make CRUD operations of the users and their roles in the json file.

The solution has too a test project (UserRolesJson.BL.Test.csproj) and is responsible to test the business layer of the application (UserRoles.BL).

Initially, the users.json file has the following users with their respective roles:
[
  {
    "Username": "John",
    "Password": "password1",
    "Roles": [
      { "Name": "PAGE_1" },
      { "Name": "PAGE_2" }
    ]
  },
  {
    "Username": "Eric",
    "Password": "password2",
    "Roles": [ { "Name": "PAGE_2" } ]
  },
  {
    "Username": "Nuria",
    "Password": "password3",
    "Roles": [ { "Name": "PAGE_3" } ]
  },
  {
    "Username": "Paula",
    "Password": "password4",
    "Roles": [
      { "Name": "PAGE_3" },
      { "Name": "ADMIN" }
    ]
  },
  {
    "Username": "Eric2",
    "Password": "password21",
    "Roles": null
  }
]

CRUD operation through the API can be tested with an API test program like POSTMAN. As mentioned the url to test the API is http://localhost:5001 
(Ej: To create a new user in the json file we make a post request with the uri http://localhost:5001/api/user/Pablo/mypassword).
To application in Visual studio must be started in order to test the API.
Only user with role ADMIN can make CRUD operation to the file. It's mandatory Basic authoritzation to can call the API (with user and pass with user 
with role admin).