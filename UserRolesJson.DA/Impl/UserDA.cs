﻿using UserRolesJson.DA.Interfaces;
using UserRolesJson.Entities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;

namespace UserRolesJson.DA.Impl
{
    public class UserDA : IUserDA
    {

        private string file = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/users.json");

        public User GetUser(string username, string password)
        {

            var userlist = this.GetAllUsers();
            var user = userlist.Where(u => u.Username.ToLower() == username.ToLower() && u.Password == password).FirstOrDefault();
            return user;
        }


        public bool UserExists(string username)
        {

            var userlist = this.GetAllUsers();
            var users = userlist.Where(u => u.Username.ToLower() == username.ToLower());
            return users.Count() > 0;
        }

        public User CreateUser(string username, string password)
        {
            var users = this.GetAllUsers();
            var user = new User
            {
                Username = username,
                Password = password
            };
            users.Add(user);
            JavaScriptSerializer ser = new JavaScriptSerializer();
            var jsonData = ser.Serialize(users);

            File.WriteAllText(file, jsonData);

            return user;

        }

        public User ModifyUser(string username, string password)
        {
            var users = this.GetAllUsers();
            var user = users.Where(u => u.Username == username).FirstOrDefault();

            if (user != null)
                user.Password = password;

            JavaScriptSerializer ser = new JavaScriptSerializer();
            var jsonData = ser.Serialize(users);

            File.WriteAllText(file, jsonData);

            return user;

        }

        public User DeleteUser(string username)
        {
            var users = this.GetAllUsers();
            var user = users.
                Where(u => u.Username.Equals(username, System.StringComparison.InvariantCultureIgnoreCase))
                .FirstOrDefault();

            if (user != null)
                users.Remove(user);

            JavaScriptSerializer ser = new JavaScriptSerializer();
            var jsonData = ser.Serialize(users);

            File.WriteAllText(file, jsonData);

            return user;

        }

        public IList<User> GetAllUsers()
        {
            string Json = File.ReadAllText(file);

            JavaScriptSerializer ser = new JavaScriptSerializer();
            var userlist = ser.Deserialize<List<User>>(Json);
            return userlist;

        }
    }
}
