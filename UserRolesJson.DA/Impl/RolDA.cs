﻿using UserRolesJson.DA.Interfaces;
using UserRolesJson.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;

namespace UserRolesJson.DA.Impl
{
    public class RolDA: IRolDA
    {

        private string file = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/users.json");



        public User AddRoleToUser(string username, string rol)
        {
            var userDA = new UserDA();
            var users = userDA.GetAllUsers();
            var user = users
                .Where(u => u.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                .First();

            if (user.Roles == null)
                user.Roles = new List<Role>();

            user.Roles.Add(new Role { Name=rol });

            JavaScriptSerializer ser = new JavaScriptSerializer();
            var jsonData = ser.Serialize(users);

            File.WriteAllText(file, jsonData);

            return user;
        }

        public User DeleteAllUserRoles(string username)
        {

            var userDA = new UserDA();

            var users = userDA.GetAllUsers();
            var user = users.
                Where(u => u.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                .First();

            user.Roles=null;

            JavaScriptSerializer ser = new JavaScriptSerializer();
            var jsonData= ser.Serialize(users);

            File.WriteAllText(file, jsonData);

            return user;

        }

     
    }
}
