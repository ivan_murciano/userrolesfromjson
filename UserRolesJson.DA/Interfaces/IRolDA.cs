﻿using UserRolesJson.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRolesJson.DA.Interfaces
{
    public interface IRolDA
    {
        User AddRoleToUser(string username, string rol);
        User DeleteAllUserRoles(string username);

    }
}
