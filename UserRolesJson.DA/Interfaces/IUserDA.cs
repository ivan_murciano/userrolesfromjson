﻿using UserRolesJson.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRolesJson.DA.Interfaces
{
    public interface IUserDA
    {
        User GetUser(string username, string password);
        User CreateUser(string username, string password);
        User ModifyUser(string username, string password);
        User DeleteUser(string username);
        bool UserExists(string username);
        IList<User> GetAllUsers();

    }
}
